import macros
import tables
import hashes
import strformat

var registers {.compileTime.} = newSeq[Table[int, string]]()

type
  Tid* = uint8
  RegisterId* = int# Index to the register in registers seq.

proc initRegister*(): RegisterId =
  registers.add(initTable[int, string]()) # Create a TableRef
  return registers.high

macro typeId*(rid: static[RegisterId], t: typed): Tid =
  let kind = getTypeImpl(t)[1]
  let name = fmt"[{kind.typeKind()}] {kind}"
  var hash = (hash(name) mod 126).int8

  let r = registers[rid].addr

  while true:
    if r[].contains(hash):
      if r[][hash] != name: # The hash key does not point to the name so recreate the hash value.
        hash = (hash *% 2) mod 126 # Make the hash more unique.
      else: break # Hash == name so return.
    else: # The register doesnt contain the hash key.
      r[][hash] = name
      break
  return (hash + 128).uint8
