# Package

version       = "0.1.0"
author        = "Matthew Murray"
description   = "Unique TypeIds inspired by yglukhov\'s variant library."
license       = "MIT"


# Dependencies

requires "nim >= 0.19.9"
